$(function(){
    $(".group").addClass("nav-active").siblings("li").removeClass("nav-active");
    $(".type-ul li").click(function(){
        $(this).addClass("type-active").siblings().removeClass("type-active");
        var mark=$(this).attr("data-mark");
        $("#mark").val(mark);
        myForm.submit();
    })
    //没有内容隐藏模块
    $(".foo").each(function(){
        var lens=$(this).find(".foo-ul li").length;
        if(lens<=0){
            $(this).hide();
        }
    })

    $(".foo-ul li").click(function(){
        var url=$(this).attr("data-url");
        window.location.href=""+url+""
    })

    //查看更多
    $(".foo").each(function(){
        var lens=$(this).find(".foo-ul li").length;
        if(lens<=6){
            $(this).find(".foo-btn").hide();
        }else{
            var pictureNum=$(this).find(".foo-ul li:gt(5)");
            pictureNum.hide();
            $(this).find(".foo-btn").click(function(){
                var picture_hide=pictureNum.is(":hidden")
                if(picture_hide){
                    pictureNum.show();
                    $(this).text("点击收起 FOLD")
                }else{
                    pictureNum.hide();
                    $(this).text("查看更多 MORE")
                };
                return false;
            })
        }
    })
})