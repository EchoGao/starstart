$(function () {
    headerEvent.logoClick()
});
//header事件集合
headerEvent = {
    //显示样式全局函数
    styleShow: function (a) {//a为页面调用函数传递参数
        let li = $('header > div ul>li');
        let logo = $('header > div .logo');
        li.eq(a).addClass('evt').siblings().removeClass('evt');
        if (a === 1) {
            li.find('>a').css('color', '#fff');
            logo.addClass('starstart');
            $('header').css('background', '#2E2E2E');
            $('footer').css('background', '#e2e2e2');
        }else if(a === 2){
            li.find('>a').css('color', '#fff');
            logo.addClass('starstart');
            $('header').addClass('dynamic-header');
            $('footer').css('background', '#e2e2e2');
        }else if(a === 3 || a === 4){
            li.find('>a').css('color', '#fff');
            logo.addClass('starstart');
            $('header').addClass('contact-header');
            $('footer').css('background','#E2E2E2');
        }
    },
    logoClick:function(){
        $('header .logo').click(function(){
            window.location.href = '/'
        })
    }
}
//cookie集合
gatherCookie = {
    //添加
    addCookie: function (name, value, expiredays) {
        var date = new Date();
        date.setTime(date.getTime() + expiredays * 24 * 3600 * 1000);
        var expires = "expires=" + date.toGMTString();
        document.cookie = name + "=" + value + ";"+ expires+"; path=/" ;//添加cookie并修改path为‘/’，确保cookie准确性
    },
    //获取
    getCookie: function (name) {
        var name = name + "=";
        var ca = document.cookie.split(";");
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i].trim();
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length)
            }
        }
        return "";
    },
    //删除
    delCookie: function (name) {
        addCookie(name, "", -1)
    }
}
