$(function(){
    headerEvent.styleShow(3);
    event.buttonEvent();
    event.showAllImages();
    event.showBigImg();
    event.hideBigImg();
});
event = {
    buttonEvent:function(){
        let argument = window.location.search.split('mark=')[1];
        $('main section ul li').eq(argument).addClass('color').siblings().removeClass('color')
    },
    showAllImages:function(){
        let button = $('main ol li button');
        let argument = window.location.search.split('mark=')[1];
        argument = (argument === '3'?3:6);
        $('main ol li').each(function(){
            let num = $(this).find('.images>div').length;

            if(num>argument){
                $(this).find('button').show();
                $(this).find('.images>div:lt('+argument+')').show();
            }else{
                $(this).find('button').hide();
                $(this).find('.images>div:lt('+argument+')').show();
            }
        });
        button.click(function(){
            if($(this).data('num') === 1){
                $(this).data('num',2);
                $(this).prev().find('>div').fadeIn();
                $(this).html($(this).data('retract'));
            }else{
                $(this).data('num',1);
                $(this).prev().find('>div:not(:lt('+argument+'))').hide();
                $(this).html('更多MCRE <em style="top:3px">﹀</em>');
            }
        })
    },
    showBigImg:function(){
        let element = $('main ol li .images > div');
        element.click(function(){
           let url = $(this).data('url');
           $('.layer-img').fadeIn().find('img').attr('src',url);
        })
    },
    hideBigImg:function(){
        $('.layer-img').click(function(){
            $(this).fadeOut();
        })
        $('.layer-img img').click(function(){
            return false;
        })
    }
}