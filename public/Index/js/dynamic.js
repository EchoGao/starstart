$(function(){
    $(".dynamic").addClass("nav-active").siblings("li").removeClass("nav-active");

    $(".type-ul li").click(function(){
        $(this).addClass("type-active").siblings().removeClass("type-active");
        var mark=$(this).attr("data-mark");
        $("#mark").val(mark);
        myForm.submit();
    })


    //    下拉刷新
    var page=1;
    $(window).bind('scroll',function(){
        var urlAjax = window.location.href;
        console.log($(document).height())
        console.log($(document).scrollTop() + $(window).height())
        if($(document).scrollTop() + $(window).height() > $(document).height()-150){
            page+=1;
            console.log(page)
            $.ajax({
                type: "POST", //用POST方式传输
                dataType: "json", //数据格式:JSON
                url: urlAjax, //目标地址
                data: {page:page},
                success: function (msg){
                    console.log(msg)
                    var news=$('.news-con');
                    if(msg.code!=='400'){
                        $('.refresh').css('display','none');
                        $('.end').css('display','block');
                        $(msg).appendTo(news);
                        $('.refresh').css('display','none');
                    }else{
                        setTimeout(function(){
                            $('.end').css('display','block');
                        }, 1000);
                    }
                }
            });
        }
    })
})