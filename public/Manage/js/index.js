$(function(){
    //侧边栏
    $("#nav-main1").addClass('nav-active');
    $("#nav-main1").find(".nav-icon-img").attr("src",'/Extend/images/aside-icon/article1.svg');
    $("#nav-main1").find('.nav-triangle').fadeIn(200);

    //表格没有内容时。。。
    var tr_len=$('.article li').length
    if(tr_len<=0){
        $('.theArticle').empty().append(
            '<img class="empty-img" src="/Manage/images/empty.svg" alt="暂无数据" title="暂无数据">'+
            '<p class="empty-font">暂时没有相关数据</p>'
        )
    }
    //清除条件
    $(".clear").click(function(){
        $("#mark").val("");
        $("#key").val("");
        myForm.submit();
    })

    //种类
    $(".to-search-type").change(function(){
        var mark=$(this).val();
        $("#mark").val(mark);
        myForm.submit();
    })

    //上传图片
    $("#headPic").click(function () {
        $("#upload").click();
        $("#upload").on("change",function(e){
            //获取图片的路径，该路径不是图片在本地的路径
            var objUrl = getObjectURL(this.files[0]);
            if (objUrl) {
                $(".display-img").show();
                $(".display-img").attr("src",objUrl) //将图片路径存入src中，显示出图片
            }
            //获取上传图片包
            var formdata = new FormData()// FormData对象，来发送二进制文件。
            formdata.append("file",e.currentTarget.files[0]);// 将文件追加到 formdata对象中
            $.ajax({
                url:'/manage/article/uploadImg',
                type:'POST',
                data:formdata,
                contentType:false,
                processData:false,
                success:function(res){
                    var thePath=res.pathname[0];
                    thePath = thePath.substr(1); //删除第一个字符
                    $(".display-img").show();
                    $(".display-img").attr("src",thePath) ; //将图片路径存入src中，显示出图片
                }
            })
        })
    });
    //保存
    $(".save").click(function(){
        var input_val = {};
        var img=$(".display-img").attr("src");
        input_val["img"] =img;
        var id=$(this).attr("data-id");
        if(id!==""){
            input_val["id"] =id;
        }
        $(".add-inp").each(function () {
            var name = $(this).attr("name");
            input_val[name] = $(this).val();
        })
        console.log(input_val)
        $.ajax({
            url:'/manage/article/inserts',
            type:'POST',
            data:input_val,
            success:function(msg){
                console.log(msg)
                if (msg.code == '1') {
                    window.location.href=window.location.href;
                } else {
                    $('.alert-info-font').text(msg.message);
                    $('.alert-info').fadeIn(300);
                    $('.alert-info-btn').on('click',function(){
                        $('.alert-info').fadeOut(300);
                    })
                }
            }
        })
    })
    //编辑
    $(".edit").click(function(){
        $('html , body').animate({scrollTop: 0},'slow');//回到顶部
        var id=$(this).attr("data-id");
        var optionsVal=$(this).attr("data-type");
        $(".save").attr("data-id",id)
        $(".add-title").val($(this).attr("data-title"));
        $(".add-url").val($(this).attr("data-url"));
        console.log($(this).attr("data-content"))
        $(".add-content").val($(this).attr("data-content"));
        $(".add-type").find("option[value = '"+optionsVal+"']").attr("selected","selected");
        $(".display-img").show();
        $(".display-img").attr("src",$(this).attr("data-img"));
    })

    //删除
    $(".delete").click(function(){
        var id=$(this).attr("data-id");
        $('.alert-info-font').text("你确定删除吗？");
        $('.alert-info').fadeIn(300);
        $('.alert-info-btn').attr('data-id',id);
        $('.alert-info-btn').addClass("alert-delete-this");
        return false;
    })
    $(".alert-info-inner").on("click", ".alert-delete-this", function () {
        var id=$(this).attr("data-id")
        console.log(id)
        $.ajax({
            type: 'post',
            url: '/manage/article/deletes',
            data:{id:id},
            success: function (msg) {
                console.log(msg)
                if (msg.code == '1') {
                    window.location.href=window.location.href;
                } else {
                    $('.alert-info-font').text(msg.message);
                    $('.alert-info').fadeIn(300);
                    $('.alert-info-btn').on('click',function(){
                        $('.alert-info').fadeOut(300);
                    })
                }
            }
        })
    })
})

//建立一個可存取到該file的url
function getObjectURL(file) {
    var url = null ;
    if (window.createObjectURL!=undefined) { // basic
        url = window.createObjectURL(file) ;
    } else if (window.URL!=undefined) { // mozilla(firefox)
        url = window.URL.createObjectURL(file) ;
    } else if (window.webkitURL!=undefined) { // webkit or chrome
        url = window.webkitURL.createObjectURL(file) ;
    }
    return url ;
}
