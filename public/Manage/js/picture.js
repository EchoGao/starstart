$(function() {
    //侧边栏
    $("#nav-main2").addClass('nav-active');
    $("#nav-main2").find(".nav-icon-img").attr("src",'/Extend/images/aside-icon/group1.svg');
    $("#nav-main2").find('.nav-triangle').fadeIn(200);

    $(".pic-li").click(function(){
        var url=$(this).attr("data-url");
        window.open(""+url+"")
    })

    //删除
    $(".delete-li").click(function(){
        var id=$(this).attr("data-id");
        console.log(id)
        $('.alert-info-font').text("你确定删除吗？");
        $('.alert-info').fadeIn(300);
        $('.alert-info-btn').attr('data-id',id);
        $('.alert-info-btn').addClass("alert-delete-this");
        return false;
    })
    $(".alert-info-inner").on("click", ".alert-delete-this", function () {
        var id=$(this).attr("data-id")
        console.log(id)
        $.ajax({
            type: 'post',
            url: '/manage/picture/deletes',
            data:{id:id},
            success: function (msg) {
                console.log(msg)
                if (msg.code == '1') {
                    window.location.href=window.location.href;
                } else {
                    $('.alert-info-font').text(msg.message);
                    $('.alert-info').fadeIn(300);
                    $('.alert-info-btn').on('click',function(){
                        $('.alert-info').fadeOut(300);
                    })
                }
            }
        })
    })

    $(".foo").each(function(){
        var len=$(this).find(".foo-ul li").length;
        if(len<=0){
            $(this).hide();
        }
    })
})



var _editor;
$(function() {
    //重新实例化一个编辑器，防止在上面的editor编辑器中显示上传的图片或者文件
    _editor = UE.getEditor('upload_ue');
    _editor.ready(function () {
        //设置编辑器不可用
        _editor.setDisabled();
        //隐藏编辑器，因为不会用到这个编辑器实例，所以要隐藏
        _editor.hide();
        //侦听图片上传
        _editor.addListener('beforeInsertImage', function (t, arg) {
            var argNum = arg.length;
            if (argNum!=0){
                var type=$(".albumType").val();
                for (var i=0;i<argNum;i++){
                    $(".sel-pic").append('<div class="img-list">' +
                        '<input class="add-val" name="type" value="' + type + '" type="hidden"/>' +
                        '<input class="add-val" name="url" value="' + arg[i].src + '" type="hidden"/>' +
                        '<img class="picture" src="' + arg[i].src+ '" >' +
                        '<img class="delete" src="/Manage/images/delete-tri.svg" onclick="delInput(this)">' +
                        '<input class="add-val pic-desc" name="content" type="text" maxlength="8" value="">'+
                        '</div>')
                }
                $("#showAlbum").show();
            }
        })
    });
});

//弹出图片上传的对话框
function upImage() {
    var myImage = _editor.getDialog("insertimage");
    myImage.open();
}
//逐个删除
function delInput(obj) {
    $(obj).parent().remove();
}


function save() {
    var input_vals=[];
    $(".img-list").each(function(){
        var input_val={};
        $(this).find(".add-val").each(function () {
            var name = $(this).attr("name");
            input_val[name] = $(this).val();
        })
        input_vals.push(input_val)
    })
    console.log(input_vals)
    $.ajax({
        type:'post',
        dataType:'json',
        url:"/manage/picture/inserts",
        data:{data:input_vals},
        success:function (msg) {
            console.log(msg)
            if (msg.code == '1') {
                window.location.href=window.location.href;
            } else {
                $('.alert-info-font').text(msg.message);
                $('.alert-info').fadeIn(300);
                $('.alert-info-btn').on('click',function(){
                    $('.alert-info').fadeOut(300);
                })
            }
        }
    })
}

