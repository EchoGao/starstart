<?php
namespace app\index\controller;
class Index extends Pre
{
    public function index()
    {
        if (request()->isMobile()){
            $info = \app\common\model\Article::order('date desc')->limit(3)->select();
            $this -> assign('info',$info);
            return view();
        }
        else{
           return view('indexPC');
        }

    }

    //新闻/动态
    public function dynamic(){
        $data = input('param.');
        if(!$data['mark']){
            $mapinfo[] = ['type','neq',''];
        }else{
            $mapinfo[] = ['type','eq',$data['mark']];
        }
        $this -> assign('data',$data);
        $result = \app\common\model\Article::where($mapinfo)
            ->order('date desc')
            ->paginate(5,false,['query'=>request()->param()]);
        $this ->assign('result',$result);
        $this ->assign('page',$result -> render());
        if (request()->isMobile()){
            if (request()->isAjax()){
                //判断数据集为空
                if ($result->isEmpty())
                    return json([
                        'status' => '400'
                    ]);
                return json($this->fetch('article-list'));
            }
            return view();
        }
        else{
            return view('dynamicPC');
        }
    }
    //集团风采
    public function group(){
        $data = input('param.');
        if(!isset($data['mark']))
            $data['mark'] = 3;
        if(in_array($data['mark'],[1,2])){
            $mapinfo[] = ['type','eq',$data['mark']];
        }else{
            $mapinfo[] = ['type','gt',2];
        }
        $result = \app\common\model\Picture::where($mapinfo) -> order('id DESC')-> select();
        if($data['mark'] == 3){
            unset($result);
            $type = array(3,4,5,6);
            foreach($type as $value){
                $mapinfo1[] = ['type','eq',$value];
                $result1 = \app\common\model\Picture::where($mapinfo1)
                            -> order('id DESC') ->select();
                $result[] = $result1 -> toArray();
                unset($mapinfo1);
            }
        }
        $result2 = \app\common\model\Picture:: order('id DESC') -> select();
        $this -> assign('info',$result2);
        $this -> assign('result',$result);
        $this -> assign('data',$data);
        if (request()->isMobile()){
            return view();
        }
        else{
            return view('groupPC');
        }
    }
    //联系我们
    public function contact()
    {
        if (request()->isMobile()){
            return view();
        }
        else{
            return view('contactPC');
        }
    }


    public function starstart()
    {
        if (request()->isMobile()){
            return view();
        }
        else{
            return view('starstartPC');
        }
    }

    public function fangsale(){
        if (request()->isMobile()){
            return view();
        }
        else{
            return view('fangsalePC');
        }
    }

    public function luozhai(){
        if (request()->isMobile()){
            return view();
        }
        else{
            return view('luozhaiPC');
        }
    }

    public function enterlo(){
        if (request()->isMobile()){
            return view();
        }
        else{
            return view('enterloPC');
        }
    }

    public function lingjun(){
        if (request()->isMobile()){
            return view();
        }
        else{
            return view('lingjun');
        }
    }

    public function ceshi(){
        $options = [
            'appid' => 'wx25ca09d3a83d4fd0',
            'appSecret' => '6a7beecab363f0f371173b8072a93dba'
        ];
        $weObj = new \wechat\Jssdk($options['appid'],$options['appSecret']);
        //微信分享
        $wechat = $weObj -> GetSignPackage();
        dump($wechat);
    }
}
