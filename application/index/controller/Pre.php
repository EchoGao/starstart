<?php
namespace app\index\controller;
//泰和
class Pre extends \think\Controller
{

    //前初始化
    protected function initialize(){
        //读取微信配置
        $options = [
            'appid' => 'wx25ca09d3a83d4fd0',
            'appSecret' => '6a7beecab363f0f371173b8072a93dba'
        ];
        $weObj = new \wechat\Jssdk($options['appid'],$options['appSecret']);
        //微信分享
        $wechat = $weObj -> GetSignPackage();
        $this -> assign('signPackage',$wechat);
    }

    //空操作
    public function _empty()
    {
        //把所有城市的操作解析到city方法
        $this -> redirect('index/index');
    }

}
