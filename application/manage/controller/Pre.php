<?php
namespace app\manage\controller;
class Pre extends \think\Controller
{
    //前初始化
    protected function initialize(){
        //验证登陆
        $info = cache(cookie('manageAuth'));
        $loginInfo = \app\common\model\Account::where('name',$info['name'])->find();
        if ($info['auth']=='manage' && $loginInfo){
            $this -> assign('loginInfo',$loginInfo);
            return true;
        }

        $this -> redirect('./manage/login/index');
    }

    //空操作
    public function _empty()
    {
        //把所有城市的操作解析到city方法
        $this -> redirect('login/index');
    }
}