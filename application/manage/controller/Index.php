<?php
namespace app\manage\controller;
class Index extends Pre
{
    //文章上传页面
    public function index()
    {
        //获取文章列表
        $data = input('param.');
        $this -> assign('data',$data);
        if ($data['key'])
            $findMap[] = ['title','like',"%$data[key]%"];
        if ($data['mark'])
            $findMap[] = ['type','eq',$data['mark']];
        $info = \app\common\model\Article::where($findMap)->order('date desc')->paginate(12,false,['query'=>request()->param()]);
        $count = \app\common\model\Article::where($findMap) -> count();
        $this -> assign('page',$info -> render());
        $this -> assign('info',$info);
        $this -> assign('count',$count);
        return view();
    }

    public function picture(){
        $result = \app\common\model\Picture::all();
        $this -> assign('info',$result);
        return view();
    }


}
