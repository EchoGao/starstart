<?php
namespace app\manage\controller;
class Login extends \think\Controller
{
    public function login(){
        $account = new \app\common\model\Account();
        return $account -> login();
    }

    //注销方法
    public function logout(){
        $account = new \app\common\model\Account();
        $account -> logout();
        $this -> redirect('./manage/index');
    }

    //登陆页面
    public function index(){
        if ($this -> isLogin())
            $this -> redirect('./manage/index');
        return view();
    }

    //判断是否登陆
    protected function isLogin(){
        $info = cache(cookie('manageAuth'));
        $check = \app\common\model\Account::where('name',$info['name'])->find();
        if ($info['auth'] == 'manage' && $check)
            return true;
    }
}