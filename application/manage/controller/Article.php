<?php
namespace app\manage\controller;
class Article
{
    //添加和更新文章
    public function inserts(){
        $article = new \app\common\model\Article;
        return $article ->inserts();
    }

    //删除文章
    public function deletes(){
        $article = new \app\common\model\Article;
        return $article ->deletes();
    }
    //图片上传
    public function uploadImg(){
        //上传文件
        $URL = "./static/upload/";
        $upload = new \app\common\logic\Upload();
        $pathName = $upload -> img('file',$URL);
        if ($pathName === false)
            return [
                'message' => '空文件'
            ];
        return [
            'code' => '1',
            'message' => '上传成功',
            'pathname' => $pathName,
        ];
    }

}
