<?php
namespace app\common\model;
class Account extends \think\Model
{
    /**
     * 验证登陆
     * @param  string  $data['name']  用户填写的账号
     * @param  string  $data['pwd']   用户填写的验证码
     */
    public function login(){
        $data = input('param.');
        $check = $this -> checkPwd($data);
        if ($check !== true)
            return [
                'message' => $check
            ];

        $token = dataSign([
            'name' => $data['name'],
            'date' => time()
        ]);
        //存入缓存
        cache($token,[
            'auth' => 'manage',
            'name' => $data['name'],
            'time' => time()
        ],3600*24*365);
        cookie('manageAuth',$token,3600*24*7);
        return [
            'code' => '1',
            'message' => '登陆成功',
            'token' => $token,
        ];
    }

    public function checkPwd($data){
        if (!$data['name'])
            return '账户名不能为空';
        if (!$data['pwd'])
            return '密码不能为空';
        $info = $this -> where('name',$data['name'])->find();
        if (!$info)
            return '该账户不存在';
        $sign = dataSign([
            'name' => $data['name'],
            'pwd' => $data['pwd']
        ]);
        if ($sign != $info['pwd'])
            return '账户名或者密码不正确';

        return true;
    }

    /**
     * 注销登陆
     * @param  string  $token  用户token
     * @return bool
     */
    public function logout(){
        cache(cookie('manageAuth'),null);
        cookie('manageAuth',null);
        return [
            'code' => '1',
            'message' => '注销成功'
        ];
    }
}