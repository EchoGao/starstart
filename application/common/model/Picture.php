<?php
namespace app\common\model;
use think\model\concern\SoftDelete;
class Picture extends \think\Model
{
    protected $resultSetType = 'collection';
    use SoftDelete;
    //添加和更新文章
    public function inserts(){
        $data = input('param.');
        if($data['data'] == '')
            return [
                'code' => '0',
                'message' => '上传信息不为空',
            ];
        $validate = validate($this -> name);

        $addData = [];
        foreach ($data['data'] as $key => $value){
            if($validate->check($value)){
                $addData[$key]['url'] = $value['url'];
                $addData[$key]['content'] = $value['content'];
                $addData[$key]['type'] = $value['type'];
                $addData[$key]['date'] = time();
            }
            else{
                return [
                    'message' => $validate->getError(),
                ];
            }

        }

        $this->saveAll($addData);
        return [
                'code' => '1',
                'message' => '添加成功',
            ];
    }
    //删除文章
    public function deletes(){
        $data = input('param.');
        $this  -> destroy($data['id']);
        return [
            'code' => '1',
            'message' => '删除成功',
        ];
    }
}