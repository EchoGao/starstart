<?php
namespace app\common\model;
use think\model\concern\SoftDelete;
class Article extends \think\Model
{
    protected $resultSetType = 'collection';
    use SoftDelete;
    //添加和更新文章
    public function inserts(){
        $data = input('param.');
        $validate = validate($this -> name);
        if(!$validate->check($data))
            return [
                'message' => $validate->getError()
            ];
        $saveData['img'] = $data['img'];
        $saveData['title'] = $data['title'];
        $saveData['url'] = $data['url'];
        $saveData['content'] = $data['content'];
        $saveData['type'] = $data['type'];
        $saveData['date'] = time();
        if($data['id']){
            $this -> where('id',$data['id']) -> update($saveData);
            return [
                'code' => '1',
                'message' => '更新成功',
                'img' => $data['img'],
            ];
        }else{
            $this -> save($saveData);
            return [
                'code' => '1',
                'message' => '添加成功',
                'id' => $data['id'],
                'img' => $data['img'],
            ];
        }
    }
    //删除文章
    public function deletes(){
        $data = input('param.');
        $this  -> destroy($data['id']);
        return [
            'code' => '1',
            'message' => '删除成功',
        ];
    }
}