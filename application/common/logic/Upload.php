<?php
namespace app\common\logic;
//上传类
class Upload
{
    /**
     * 上传文件
     * @param  string  $fileName  上传的文件名
     * @param  string  $fileName  重新命文件名
     * @param  string  $URL       文件路径
     * @param  bool  $water       添加水印
     */
    public function img($fileName,$URL,$water=false){
        $files = request()->file($fileName);
        // 获取表单上传文件
        if ($files){
            if (!is_array($files))
                $files = [$files];
            $pathNameArr = [];
            foreach ($files as $file){
                //源文件
                $info = $file -> move($URL);
                $saveName = $info->getSaveName();
                //过滤视频
                if ($water){
                    $check = explode('.',$saveName);
                    if(!in_array($check[1],['mp4'])){
                        // 按照原图的比例生成一个最大为150*150的缩略图并保存为thumb.png
                        $image = \think\Image::open($info);
                        $image->thumb(610, 610)->save($URL.str_replace(basename($saveName),'thumb_'.basename($saveName),$saveName));
                    }
                }
                $pathNameArr[] = $URL.$saveName;
            }
            return $pathNameArr;
        }

        return false;

    }

    public function video($fileName){
        $file = request() ->file($fileName);
        if ($file){
            $url = "./static/upload/building/video/";
            $info = $file->validate(['size'=>100*1024*1024,'ext'=>'mp4']) -> move($url);
            if($info){
                $pathName = $url.$info->getSaveName();
                return [
                    'code' => '1',
                    'pathName' => $pathName
                ];
            }
            else{
                return [
                    'message' => $file->getError()
                ];
            }

        }
    }

}