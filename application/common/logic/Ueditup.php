<?php
namespace app\common\logic;
class Ueditup
{
    //uEditor上传入口
    public function index(){
        $data = input('param.');
        //前台判断
        $editconfig = array('imageActionName'=>'uploadimage','imageFieldName'=>'upfile',"imageMaxSize"=>'5242880','imageAllowFiles'=>array('.png','.jpg','.jpeg','.webp'),'imageUrlPrefix'=> '','imageManagerActionName'=>'listimage','imageManagerUrlPrefix'=>'','videoActionName'=>'uploadvideo','videoFieldName'=>'upfile','videoAllowFiles'=>array('.mp4'),'videoUrlPrefix'=> '');
        switch ($data['action']) {
            case 'config':
                $result =  json($editconfig);
                break;
            /* 上传图片*/
            case 'uploadimage':
                $result = $this->editupImg($data['auth']);
                break;
            /* 列出图片 */
            case 'listimage':
                $result = json($this->editlist($data['auth']));
                break;
            /* 删除图片命令处理 */
            case 'deleteimage':
                $result = $this->action_delete();
                break;
            /* 上传视频*/
            case 'uploadvideo':
                $result = $this->editupImg($data['auth']);
                break;
            default:
                $result = json(array(
                    'state'=> '请求地址出错'
                ));
                break;
        }
        /* 输出结果 */
        return $result;
    }

    //uEditor上传图片
    public function editupImg($auth)
    {
        $url = "./static/upload/picture/$auth/";
        $upload = new Upload();
        $pathName = $upload -> img('upfile',$url);


        $_re_data['state'] = 'SUCCESS';
        $_re_data['url'] = ltrim($pathName[0],'.');
        $_re_data['title'] = basename($pathName[0]);
        $_re_data['original'] = '1';
        $_re_data['type'] = '.' .'1';
        $_re_data['size'] = '1';
        return json($_re_data);
    }

    //uEditor列出图片
    public  function editlist($auth)
    {
        $dir = "./static/upload/picture/$auth";
        $listFile=array();
        if(is_dir($dir)){
            if($handle=opendir($dir)){
                while(($file=readdir($handle))!==false){
                    if($file!='.' && $file!=".."){
                        $fileContent = scandir($dir.'/'.$file);
                        foreach ($fileContent as $value){
                            if ($value!='.' && $value!='..')
                                $listFile[$file][] = $value;
                        }
                    }
                }
            }
        }
        closedir($handle);
        $files = [];
        foreach ($listFile as $k=> $value){
            foreach ($value as $item){
                $check = explode('.',$item);
                //不返回视频格式
                if (!in_array($check[1],['mp4'])){
                    //不返回缩略图
                    if (strpos($item,'thumb_') === false){
                        $b = [[
                            'url' => "/static/upload/picture/$auth/$k/$item" ,
                            'mtime' => '1'
                        ]];
                        $files = array_merge($files,$b);
                    }
                }
            }
        }


        /* 返回数据 */
        $result = array(
            "state" => "SUCCESS",
            "list" => $files,
            "start" => 0,
            "total" => count($files)
        );

        return $result;
    }


    //uEditor删除图片
    public  function action_delete(){
        //获取路径
        $path = input('post.path');
        unlink(".$path");
        return 'ok';
    }
}