<?php
namespace app\common\validate;
class Picture extends \think\Validate
{
    protected $rule = [
        'url' => 'require|unique:Picture',
        'content' => 'require',
        'type' => 'require',
    ];

    protected $message = [
        'url.require' => '图片路径不能为空',
        'url.unique' => '图片重复',
        'content.require' => '描述信息不能为空',
        'type.require' => '类型不能为空',
    ];
}