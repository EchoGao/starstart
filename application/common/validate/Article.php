<?php
namespace app\common\validate;
class Article extends \think\Validate
{
    protected $rule = [
        'title'  => 'require',
        'url' => 'require|unique:Article',
        'content' => 'require',
        'type' => 'require',
        'img' => 'require',
    ];

    protected $message = [
        'img.require' => '宣传图片不能为空',
        'title.require' => '推送标题不能为空',
        'url.require' => '推送链接不能为空',
        'url.unique' => '推送链接不能重复',
        'content.require' => '描述信息不能为空',
        'type.require' => '资讯类别不能为空',
    ];
}